This is an updated version of the [graphstream-geography depot](https://github.com/graphstream/gs-geography) to insure that the system is still working.

 - It supports gs 2.0-alpha, java 8, and I added the conversion from pbf 2 osm
  - See Dedale's website https://dedale.gitlab.io/page/sisterprojects/gs-geography/ for details.
 
--

GraphStream
===========

Welcome to GraphStream, and thank you for your download, we hope you will find it
useful and practical.

We are always interested by suggestions on how to make the library more developer
friendly, see the website (www.graphstream-project.org) for more information.

Geography
---------

This is a module with tools to deal with GIS data and other classes of interest
mainly for geographers. This is a work in progress and there is yet no jar
available.

Authors
-------

The GraphStream team:

Julien Baudry <julien.baudry@graphstream-project.org>
Antoine Dutot <antoine.dutot@graphstream-project.org>
Yoann Pigné <yoann.pigne@graphstream-project.org> 
Guilhelm Savin <guilhelm.savin@graphstream-project.org>


Contributors:

Frédéric Guinand <frederic.guinand@univ-lehavre.fr>
Guillaume-Jean Herbiet <guillaume-jean@herbiet.net>


Help
----

You may check the documentation on the website (http://graphstream-project.org). 
You may also share your questions on the mailing list at 
http://sympa.litislab.fr/sympa/subscribe/graphstream-users 


Copyright
---------

GraphStream is distributed under the GPL v2.
