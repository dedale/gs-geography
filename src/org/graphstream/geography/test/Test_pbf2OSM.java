package org.graphstream.geography.test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.topobyte.osm4j.core.access.OsmIterator;
import de.topobyte.osm4j.core.access.OsmOutputStream;
import de.topobyte.osm4j.core.model.iface.EntityContainer;
import de.topobyte.osm4j.core.model.iface.OsmNode;
import de.topobyte.osm4j.core.model.iface.OsmRelation;
import de.topobyte.osm4j.core.model.iface.OsmWay;
import de.topobyte.osm4j.pbf.seq.PbfIterator;
import de.topobyte.osm4j.xml.output.OsmXmlOutputStream;

public class Test_pbf2OSM {
	
	
	public static void main(String[] args) throws IOException	  {
		System.out.println("PBF 2 OSM  in progress..");
	    // Open a file as input
	    InputStream input = new FileInputStream(System.getProperty("user.dir")+"/data/antarctica-latest.osm.pbf");
	 
	    // Create an iterator for PBF data
	    OsmIterator iterator = new PbfIterator(input, true);
	 
	    OutputStream os2= new FileOutputStream(System.getProperty("user.dir")+"/data/antarctica.osm", true);
	    // Create an output stream
	    OsmOutputStream osmOutput = new OsmXmlOutputStream(os2, true);//System.out
	 
	    // Iterate objects and copy them to the output
	    for (EntityContainer container : iterator) {
	      switch (container.getType()) {
	      default:
	      case Node:
	        osmOutput.write((OsmNode) container.getEntity());
	        break;
	      case Way:
	        osmOutput.write((OsmWay) container.getEntity());
	        break;
	      case Relation:
	        osmOutput.write((OsmRelation) container.getEntity());
	        break;
	      }
	    }
	 
	    // Close output
	    osmOutput.complete();
	    System.out.println("PBF 2 OSM successfully done. \n File written : ");//+fileNameFullPath.substring(0,fileNameFullPath.length()-4).concat(".dgs"));
	  }
}
